Fyber Data Challenge
====================

At Fyber we are showing ads to the user, which will be recorded by an event in Kafka.

The event has the following and other fields:

```
name	                type	comment
=======================================
request_id	            string	Unique ID which connects all events which originated from the same container request.
integration	            string	name of integration
application_id	        bigint	Application ID from Fyber database of applications.
application_user_id	    string	User ID in Application may be not unique
device_id	            string	Unique identifier of device. It is the first available identifier from the list: `apple_idfa`, `google_ad_id`, `android_id`, `mac_address`, `cookie`.
user_country_code 	    string	Country of user. 2 uppercase letters code from ISO 3166-1 alpha-2 standard
user_language	        string	Language of user. 2 uppercase letters code from ISO 3166-1 alpha-2 standard
ip_address	            string	IP address of dev
advertisement_id	    string	Unique ID for Ad
provider	            string	Identifier of the provider which delivered the ad.
trackable	            boolean	Indicates if event should be used for calculations
placement_identifier	string	Identifier set by the publisher to uniquely identify the placement.
ad_format	            string	name of the advertisement format
```

You task is to calculate the `unique impression count` for a given data set of this events, which is defined like this:

- Every event is counted only once per `application_id`, `user_country_code`, `integration`, `ad_format`, `device_id` combination, this count should represented by a `count` column in the result.
- In addition to this grouping, the counts should be split up by the values of the  `provider` and `placement_identifier` column
- There should be a row with the count of all events per `provider` and `placement_identifier`, in that case the value for `placement_identifier` should be `%ALL%`, for `provider` it should be empty string `''`
- If the `device_id` contains the value `00000000-0000-0000-0000-000000000000`, null or is empty, the event should be counted by the `application_user_id` instead.
- Events with `trackable` attribute set to `false` should not be counted.
- Rows with null-values should be dropped

So for an input like
```
"request_id", "application_id", "ad_format", "user_country_code", "integration", "trackable", "provider", "device_id", "application_user_id", "publisher_revenue_source_type", "placement_identifier"
"2", 100L, "video", "FR", "mobile_brandengage", true, "Fyber", "dev0", "3", "sponsorpay", "home",
"3", 300L, "video", "FR", "mobile_brandengage", true, "Fyber", "dev1", "3", "sponsorpay", "home",
"4", 300L, "video", "FR", "mobile_brandengage", true, "Fyber", "dev2", "3", "sponsorpay", "home",
"4.0", 300L, "video", "FR", "mobile_brandengage", true, "Fyber", "dev2", "3", "sponsorpay", "",
"4.1", 300L, "video", "FR", "mobile_brandengage", true, "AdMob", "dev2", "3", "sponsorpay", "",
"4.2", 300L, "video", "FR", "mobile_brandengage", true, "Vungle", "dev2", "3", "sponsorpay", "",
"5", 300L, "video", "FR", "mobile_brandengage", true, "AdMob", "dev3", "3", "sponsorpay", "home",
"6", 300L, "video", "FR", "mobile_brandengage", true, "AdMob", "dev4", "3", "sponsorpay", ""
```

The result should look like
```
"application_id", "user_country_code", "integration", "ad_format", "provider", "count", "placement_identifier"
100L, "FR", "mobile_brandengage", "video", "fyber", 0, "",
100L, "FR", "mobile_brandengage", "video", "fyber", 1, "home",
100L, "FR", "mobile_brandengage", "video", "", 1, "%ALL%",
100L, "FR", "mobile_brandengage", "video", "fyber", 1, "%ALL%",
300L, "FR", "mobile_brandengage", "video", "fyber", 2, "%ALL%",
300L, "FR", "mobile_brandengage", "video", "vungle", 1, "%ALL%",
300L, "FR", "mobile_brandengage", "video", "", 4, "%ALL%",
300L, "FR", "mobile_brandengage", "video", "admob", 3, "%ALL%",
300L, "FR", "mobile_brandengage", "video", "fyber", 2, "home",
300L, "FR", "mobile_brandengage", "video", "admob", 2, "",
300L, "FR", "mobile_brandengage", "video", "fyber", 1, "",
300L, "FR", "mobile_brandengage", "video", "vungle", 1, "",
300L, "FR", "mobile_brandengage", "video", "admob", 1, "home"
```

You solution should be implemented using Apache Spark and Scala (no particular version is required).

The [example data](events.zip) will be given as a set of parquet files, split by hour. There are standard methods in Spark to read this format.
